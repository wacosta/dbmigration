# Application that allows migrating tables from two different databases

Download: https://wacosta.gitlab.io/dbmigration/launch.html

For Test, add domain access in Security Java Control:
https://wacosta.gitlab.io

![](img/dbMigration.PNG)