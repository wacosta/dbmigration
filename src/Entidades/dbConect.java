/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author wacosta
 */
public class dbConect {

    static Logger logger = Logger.getLogger(dbConect.class.getName());

    public static Connection init(String origen, String servidor, int port, String DB, String user, String pass) {
        Connection connection = null;
        String connectionUrl = "";
        try {
            switch (origen) {
                case "Oracle":  //Requiere tnsnames.ora
                    Class.forName("oracle.jdbc.driver.OracleDriver");
                    connectionUrl += "jdbc:oracle:thin:" + user + "/" + pass + "@" + servidor + ":" + port + ":" + DB;
                    break;
                case "SQL Server":
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    connectionUrl += "jdbc:sqlserver://" + servidor + ":" + port + ";database=" + DB + ";user=" + user + ";password=" + pass + ";";
                    break;
                case "MySQL":
                    Class.forName("com.mysql.jdbc.Driver");
                    connectionUrl += "jdbc:mysql://" + servidor + ":" + port + "/" + DB + "?user=" + user + "&password=" + pass+"&relaxAutoCommit=true";
                    break;
            }
            logger.log(Level.INFO, "Conexi\u00f3n Establecida: {0}", connectionUrl);
            connection = DriverManager.getConnection(connectionUrl);
        } catch (ClassNotFoundException | SQLException e) {
            logger.log(Level.SEVERE, "---> [ERROR] {0}", e);
        }
        return connection;
    }

    public static void desconectar(Connection con) {
        try {
            con.close();
            logger.log(Level.INFO, "Desconectado");
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "---> [ERROR] {0}", e);
        }
    }

    public static ResultSet getDatos(Connection conn, String tabla) {
        ResultSet resultSet = null;
        String sql = "SELECT * FROM " + tabla;
        try {
            Statement statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "---> [ERROR] {0}", e);
        }
        return resultSet;
    }

    public static ResultSet setDatos(Connection conn, ResultSet origen, String tabla) {
        ResultSet resultSet = null;        
        try {
            ResultSetMetaData rsmd = origen.getMetaData();
            String header = "INSERT INTO " + tabla + " (";
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                if (i == rsmd.getColumnCount()) {
                    header += rsmd.getColumnName(i) + ") ";
                } else {
                    header += rsmd.getColumnName(i) + ", ";
                }
            }
            header += "VALUES (";
            while (origen.next()) {
                String sql = header;
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    if (i == rsmd.getColumnCount()) {
                        try {
                            sql += origen.getInt(i) + ");";
                        } catch (Exception e) {
                            String text = origen.getString(i);
                            String replace1 = text.replace("'", ""); //Solve bug of quotes mark
                            String replace2 = replace1.replace("\"", "");
                            sql += "'" + replace2 + "');";
                        }
                    } else {
                        try {
                            sql += origen.getInt(i) + ",";
                        } catch (Exception e) {
                            String text = origen.getString(i);
                            String replace1 = text.replace("'", "");
                            String replace2 = replace1.replace("\"", "");
                            sql += "'" + replace2 + "',";
                        }
                    }
                }
                PreparedStatement prepsInsert = conn.prepareStatement(sql);
                prepsInsert.execute();
                resultSet = prepsInsert.getResultSet();
                logger.log(Level.INFO, "[INFO]: {0}", sql);
                conn.commit();
            }            
        } catch (Exception e) {
            logger.log(Level.SEVERE, "---> [ERROR] {0}", e);
            try {
                conn.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(dbConect.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return resultSet;
    }

    public static ResultSet deleteDatos(Connection conn, String tabla) {
        ResultSet resultSet = null;
        String sql = "TRUNCATE TABLE " + tabla;
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
            logger.log(Level.INFO, "[INFO]: {0}", sql);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "---> [ERROR] {0}", e);
        }
        return resultSet;
    }
}
